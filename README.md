# ToolsToKeepSomewhere

## Tools / Software

* [Gitbook](https://www.gitbook.com) - Documentation tool
* [Hotjar](https://www.hotjar.com/) - Analytics and user feedback service
* [FontSpark](https://fontspark.app/) - Font helper
* [Animate.css](https://daneden.github.io/animate.css/) - Animation package

## Image Bank
* [Lukaszadam](https://lukaszadam.com/illustrations)
* [UnDraw](https://undraw.co/)

## Blogs / Forums

* [DEV](https://www.dev.to) - IT Blog
* [malcoded](https://malcoded.com/) - Tutos about : Angular / Vue / React

## Template

* [Creative Tim](https://www.creative-tim.com/) - Template Bootstrap / Material for Angular / Vue / React / Laravel
* [unDraw](https://undraw.co/) - Open-source illustrations library
* [StartBootstrap](https://startbootstrap.com) - Templates and snippets
* [CssTricks](https://css-tricks.com) - Templates / snippets / video for HTML / CSS / JS tricks and tips

## Events

* [HacktoberFest](https://hacktoberfest.digitalocean.com/) - Open source challenge  

## Techno focus

### Git

- [Git: Cheat Sheet (Advanced) ](https://dev.to/maxpou/git-cheat-sheet-advanced-3a17)
- [Add existing Git repo to Gitlab](https://stackoverflow.com/questions/20359936/import-an-existing-git-project-into-gitlab)

### Vue

- [Fix problem when importing some component/librairies](https://stackoverflow.com/questions/49258849/could-not-find-a-declaration-file-for-module-vue-xxx/49259337)

### JavaScript

 - [List of design / animation librairies](https://dev.to/paco_ita/be-more-productive-with-these-tools-september-picks-for-you-2efm)

### Angular

 - [Tuto Angular OpenClassroom](https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular)
 - [Example of project using: Angular / Express / MongoDB](https://github.com/KrunalLathiya/Angular7CRUDExample)
 - [Bootstrap for Angular](https://ng-bootstrap.github.io/#/home)
 - [Material for Angular](https://material.angular.io/)

### VueJS

- [Vue-CLI](https://cli.vuejs.org)
- [Tuto internationalization](https://www.freecodecamp.org/news/how-to-add-internationalization-to-a-vue-application-d9cfdcabb03b/) - Multi language
- Snackbars

## Acknowledgments

![Alt text](butterfly.png)
